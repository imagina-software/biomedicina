﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Biomedicina.Models
{
    public class Login
    {

        [Required(ErrorMessage = "Por favor ingrese un nombre de usuario")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Por favor ingrese un password")]
        public string Password { get; set; }
    }
}