﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Biomedicina.Models
{
    public class ProductoViewModel
    {

        public List<Producto> lstProducto { get; set; }

        public List<Empresa> lstEmpresa { get; set; }

        public List<Cotizacion> lstCotizacion { get; set; }

        /*Cotizacion*/
        public int IdCotizacion { get; set; }

        public int IdProducto { get; set; }

        public int IdUsuario { get; set; }

        public string Mensaje { get; set; }

        public Cliente Cliente { get; set; }

        public Login Login { get; set; }
    }
}