﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biomedicina.Models
{
    public class Cotizacion
    {

        public int IdCotizacion { get; set; }

        public int IdProducto { get; set; }

        public int IdUsuario { get; set; }

        public string Mensaje { get; set; }

    }
}