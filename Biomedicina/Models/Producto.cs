﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biomedicina.Models
{
    public class Producto
    {

        public int Id { get; set; }

        public string Nombre { get; set; }

        public int Id_categoria { get; set; }

        public string Nombre_categoria { get; set; }

        public string Nombre_empresa { get; set; }

        public string Modelo { get; set; }

        public string Descripcion { get; set; }

        public string Espec_tecnica { get; set; }

        public string Url_ficha_tecnica { get; set; }

        public string Imagen_url { get; set; }

        public string Capacidad { get; set; }

        public string Dimension { get; set; }

        public string Temporizador { get; set; }

        public string Temperatura { get; set; }

        public string Dimension_tanque { get; set; }

        public string Potencia { get; set; }

        public string Energia { get; set; }

    }
}