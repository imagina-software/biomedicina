﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Biomedicina.Models
{
    public class Especificacion_tecnica
    {

        public string Capacidad { get; set; }

        public string Temporizador { get; set; }

        public string Temperatura { get; set; }

        public string Dimension_tanque { get; set; }

        public string Potencia { get; set; }

        public string Energia { get; set; }

    }
}