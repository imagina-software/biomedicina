﻿var JqueryUiForms = function () {

    // Autocomplete Productos
    var _componentUiAutocomplete = function () {

        if (!$().autocomplete) {
            console.warn('Warning - jQuery UI components are not loaded.');
            return;
        }
      
        $("#Nombre").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "/Home/AutoComplete_CategoriaProductos",
                    type: "POST",
                    dataType: "json",
                    data: { Prefix: request.term },
                    success: function (data) {
                        response($.map(data, function (item) {
                            return {
                                label: item.Nombre_categoria,
                                value: item.Nombre_categoria,
                                categ: item.Id_categoria
                            };
                        }))

                    }
                })
            },
            messages: {
                noResults: "", results: ""
            },
            search: function () {
                $(this).parent().addClass('ui-autocomplete-processing');
            },
            open: function () {
                $(this).parent().removeClass('ui-autocomplete-processing');
            }, 
            select: function (e, i) {
                
                $("#categoria_producto").val(i.item.categ);
            },
            minLength: 3
        });

      
    };

    return {
        init: function () {
            _componentUiAutocomplete();
       
        }
    }


}();

document.addEventListener('DOMContentLoaded', function () {
    JqueryUiForms.init();
});


$(function () {
 
    $("#btnBuscar").click(function () {
       

        //if (!$("#RegistrarUsuarioForm").valid()) {

        //    return false;
        //}

        //var pregunta = confirm("¿Desea grabar la información ingresada?");

        //if (pregunta)
            buscarProductos();

    });


    $("#btnRegistrarCotizacion").click(function () {



        //if (!$("#RegistrarServicioForm").valid()) {

        //	return false;
        //}


        var pregunta = confirm("¿Desea grabar la información ingresada?");

        if (pregunta)
            //registrarServicio(parametros);
            registrarCotizacion();


    });


});

function f_filtrar_x_empresa(id_categoria) {

    var arrItem = [];
    var commaSeparateIds = "";

    $("#ItemList input[type=checkbox]").each(function (index,val) {
        debugger;

        var checkId = $(val).attr("Id");
        var arr = checkId.split('_');
        var currentCheckboxId = arr[1];
        var IsChecked = $('#' + checkId).is(":checked", true);

        if (IsChecked) {

            arrItem.push(currentCheckboxId);
        }

    })


    if (arrItem.length !=0) {

        commaSeparateIds = arrItem.toString();
        var dataToSend = 'idEmpresa=' + commaSeparateIds + '&idCategoria='+ id_categoria;

        $.ajax({
            url: "/Productos/FiltrarXEmpresa",
            type: "POST",
            data: dataToSend,
            dataType: "html",
            success: function (result) {
                debugger
                //waitingDialog.hide();
                $("#divResultadosProductos").html(result);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                alert(errorThrown);
            }
        });

    }



    //var infoForm = $("#frmFiltros");    
    //var infoForm = $("#frm_busqueda_result");

  
}




function openModalCotizacion(idProducto) {


    var dataToSend = 'idProducto=' + idProducto;

    $.ajax({
        type: "POST",
        url: "/Productos/OpenModalCotizacion",
        data: dataToSend,
        //contentType: "application/json; charset=utf-8",
        //dataType: "json",
        success: function (result) {

            $("#cotizacionContainer").html(result);
            //alert("Hello: " + response.Name + " .\nCurrent Date and Time: " + response.DateTime);
            //alert("Hello: ");
        },
        failure: function (response) {

            alert(response.responseText);
        },
        error: function (jqXHR, textStatus, errorThrown) {

            alert("Req: " + jqXHR);
            alert("Status: " + textStatus);
            alert("Error: " + errorThrown);
        },
        complete: function () {

            $('#modal_form_horizontal').modal('show');
        }
    });

}


function registrarCotizacion() {

    var dataToSend = 'pIdproducto=' + "GG";


    $.ajax({
        type: "POST",
        url: "/Productos/RegistrarCotizacion",
        data: dataToSend,
        //data: JSON.stringify(parametros),
        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
        dataType: "json",
        success: function (response) {
            var rpta = response.indicador;
            var mensaje = response.contenido;

            if (rpta == "OK") {
                //limpiar();
                //listarServicio(0);


            }
            else
                alert(mensaje);
        },
        failure: function (response) {
            alert(response.responseText);
        },
        error: function (response) {
            alert(response.responseText);
        }
    });
}


function updateAddressSuccess() {

    $("#cotizacionContainer").html(result);
}


function success() {
    alert("ok");
}
var onSuccessResult = function () {
    alert("gg");
    // Un pequeño esfecto especial ;)
    $("#PostList .row").first().hide();
    $("#PostList .row").first().slideToggle("slow");

    // Limpia el formulario
    $("#Comentario").val("");
    $("#Nombre").val("");
    $("#Email").val("");
    $("#Titulo").val("");
    // Otra forma de limpiar el Formulario
    //$("#AjaxForm")[0].reset();
    //$("#AjaxForm").trigger('reset');

    // Escondemos el Ajax Loader
    $("#AjaxLoader").hide("slow");

    // Habilitamos el botón de Submit
    $("#SubmitBtn").prop("disabled", false);

    // Mostramos un mensaje de éxito.
    $("#ExitoAlert").show("slow").delay(2000).hide("slow");
};
//function buscarProductos() {

//    debugger

//    $.ajax({
//        type: "POST",
//        url: "/Home/ResultadosProducto",
//        data: $("#BuscarProductoForm").serialize(),
//        //data: JSON.stringify(parametros),
//        contentType: "application/x-www-form-urlencoded; charset=UTF-8",
//        dataType: "json",
//        success: function (response) {
//            window.location.href = "Home/ResultadosProducto";

//        },
//        failure: function (response) {
//            alert(response.responseText);
//        },
//        error: function (response) {
//            alert(response.responseText);
//        }
//    });

//}