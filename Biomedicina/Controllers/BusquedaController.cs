﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Biomedicina.Repository.Biomedicina;
using Biomedicina.Models;

namespace Biomedicina.Controllers
{
    public class BusquedaController : Controller
    {

        ProductoViewModel objProductoViewModel = new ProductoViewModel();
        // GET: Busqueda
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult BuscarProducto(Producto objProducto) {


            List<Producto> lista;
            BusquedaAccess objBusqueda = new BusquedaAccess();

            lista = objBusqueda.BuscarProductoxCategoria(objProducto);

            objProductoViewModel.lstProducto = lista;

            return View("ResultadosProducto", objProductoViewModel);
        }

        public ActionResult ResultadosProducto(Producto objProducto) {


            List<Producto> lista;
            BusquedaAccess objBusqueda = new BusquedaAccess();

            lista = objBusqueda.BuscarProductoxCategoria(objProducto);

            objProductoViewModel.lstProducto = lista;

            return View(objProductoViewModel);

        }

        public ActionResult FiltrarXEmpresa()
        {



            return View();
        }

    }
}