﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Biomedicina.Models;
using Biomedicina.Repository.Biomedicina;

namespace Biomedicina.Controllers
{
    public class LoginController : Controller
    {

        ProductoViewModel objProductoViewModel = new ProductoViewModel();
        RegistroAccess objRegistro = new RegistroAccess();


        // GET: Login
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Login(ProductoViewModel objLogin )
        {

            LoginAccess objLoginAccess = new LoginAccess();

            if (ModelState.IsValid)
            {

                var obj = objLoginAccess.Login(objLogin.Login.UserName, objLogin.Login.Password);


                if (obj.Count() > 0)
                {

                    Session["UserID"] = obj.FirstOrDefault().UserId;
                    Session["UserName"] = obj.FirstOrDefault().UserName;
                    Session["Nombre"] = obj.FirstOrDefault().Nombre;

                    ViewBag.OcultarModal = true;
                    ModelState.Clear();

                }
                else
                {

                    ViewBag.Message = "Usuario o password invalido";
                    ViewBag.OcultarModal = false;
                    return PartialView("~/Views/Login/_FormLogin.cshtml", objLogin);


                }

            }
            else{

                ViewBag.OcultarModal = false;

            }



            return PartialView("~/Views/Login/_FormLogin.cshtml", objLogin);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult RegistrarCliente(ProductoViewModel objCliente)
        {

            String mensaje = "";
            String resultado = "";
            String valid = "";
            String fecha = "";
            String accion = "";
    
            try
            {
                if (ModelState.IsValid)
                {

                    /****Logica para grabar la cotizacion****/
                    mensaje = objRegistro.RegistrarCliente(objCliente);

                    if (mensaje.Equals(string.Empty)) {
                        ViewBag.FlagMensaje = "OK";
                        mensaje = "Se ha registrado correctamente.";

                    }

                    ViewBag.Message = mensaje;

                    ModelState.Clear();
                    //transaccion.Complete();

                    resultado = "OK";
                    return PartialView("~/Views/Login/_FormRegistroUsuario.cshtml", objCliente);

                }

                return PartialView("~/Views/Login/_FormRegistroUsuario.cshtml", objCliente);

            }

            catch (Exception ex)
            {

                mensaje = ex.Message;
                resultado = "ER";
                return View("~/Views/Shared/Error.cshtml");

            }
            //}
        }

        //public JsonResult Login2(Login objUser)
        //{

        //    LoginAccess objLogin = new LoginAccess();

        //    if (ModelState.IsValid)
        //    {

        //        var obj = objLogin.Login(objUser.UserName, objUser.Password);

        //        //var obj = db.UserProfiles.Where(a => a.UserName.Equals(objUser.UserId) && a.Password.Equals(objUser.Password)).FirstOrDefault();
        //        if (obj.Count() > 0)
        //        {

        //            Session["UserID"] = obj.FirstOrDefault().UserId;
        //            Session["UserName"] = obj.FirstOrDefault().UserName;
        //            Session["Nombre"] = obj.FirstOrDefault().Nombre;

        //            return RedirectToAction("Index", "Home");
        //        }
        //        else
        //        {

        //            ViewBag.Message = "Usuario o password invalido";
        //            return PartialView("~/Views/Login/_ModalLogin.cshtml", objUser);

        //        }


        //    }

        //    return PartialView("~/Views/Login/_ModalLogin.cshtml", objUser);
        //}

        public ActionResult UserDashBoard()
        {
            if (Session["UserID"] != null)
            {
                return View();
            }
            else
            {
                return RedirectToAction("Login");
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            Session.Remove("UserID");
            return RedirectToAction("Index", "Home");
        }

    }
}