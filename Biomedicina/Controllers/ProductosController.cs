﻿using Biomedicina.Models;
using Biomedicina.Repository.Biomedicina;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Transactions;

namespace Biomedicina.Controllers
{
    public class ProductosController : Controller
    {

        ProductoViewModel objProductoViewModel = new ProductoViewModel();
        BusquedaAccess objBusqueda = new BusquedaAccess();
        RegistroAccess objRegistro = new RegistroAccess();


        // GET: Productos
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Resultados(Producto objProducto)
        {

            List<Producto> lista_producto;
            List<Empresa> lista_empresa;


            lista_producto = objBusqueda.BuscarProductoxCategoria(objProducto);
            lista_empresa = objBusqueda.ListarEmpresa();

            objProductoViewModel.lstProducto = lista_producto;
            objProductoViewModel.lstEmpresa = lista_empresa;

            return View(objProductoViewModel);

        }

        public ActionResult FiltrarXEmpresa(string idEmpresa,string idCategoria)
        {

            List<Producto> lista_producto = new List<Producto>();

            List<Producto> lista_producto_acumulado = new List<Producto>();

            string[] arr = idEmpresa.Split(',');

            foreach (var id in arr) {

                lista_producto = objBusqueda.FiltrarProductoXEmpresa(Convert.ToInt32(id), Convert.ToInt32(idCategoria));

                lista_producto_acumulado.AddRange(lista_producto);

                objProductoViewModel.lstProducto = lista_producto_acumulado;
            }

            return PartialView("~/Views/Productos/_Resultados.cshtml", objProductoViewModel);

        }

        [HttpPost]
        public ActionResult OpenModalCotizacion(string idProducto) {

            try
            {

                List<Producto> lista_producto;
                Cotizacion objCotizacion = new Cotizacion();

                lista_producto = objBusqueda.BuscarProductoxId(Convert.ToInt32(idProducto));

                objProductoViewModel.lstProducto = lista_producto;
                ViewBag.lstProductoSelect = lista_producto;

                return PartialView("~/Views/Productos/_ModalCotizacion.cshtml", objProductoViewModel);

            }
            catch (Exception ex) 
            {
                return View("~/Views/Shared/Error.cshtml");

            }

        }

        [HttpPost]
        public ActionResult RegistrarCotizacion(ProductoViewModel objProducto)
        {

            String mensaje = "";
            String resultado = "";
            String valid = "";
            String fecha = "";
            String accion = "";
            List<Producto> lista_producto;


            //using (TransactionScope transaccion = new TransactionScope())
            //{

                try
                {
                    if (ModelState.IsValid)
                    {



                        /****Logica para grabar la cotizacion****/

                        mensaje=objRegistro.RegistrarCotizacion(objProducto);


                    if (mensaje.Equals("OK"))
                    {
                        ViewBag.Message = "Se envio la cotizacion";


                    }
                    else {

                        ViewBag.Message = mensaje;

                    }


                    lista_producto = objBusqueda.BuscarProductoxId(Convert.ToInt32(objProducto.IdProducto));

                        objProductoViewModel.lstProducto = lista_producto;

                        mensaje = "Se grabo correctamente la información.";

                        ModelState.Clear();
                        //transaccion.Complete();

                        resultado = "OK";
                        return PartialView("~/Views/Productos/_FormCotizacion.cshtml", objProductoViewModel);

                    }

                    lista_producto = objBusqueda.BuscarProductoxId(Convert.ToInt32(objProducto.IdProducto));

                    objProductoViewModel.lstProducto = lista_producto;

                    return PartialView("~/Views/Productos/_FormCotizacion.cshtml", objProductoViewModel);


                }

                catch (Exception ex)
                {
                    //transaccion.Dispose();

                    mensaje = ex.Message;
                    resultado = "ER";
                    return View("~/Views/Shared/Error.cshtml");


                }

            //}
           
        }

        [HttpPost]
        public JsonResult RegistrarCotizacion2(ProductoViewModel objProducto)
        {

            String mensaje = "";
            String resultado = "";
            String valid = "";
            String fecha = "";
            String accion = "";


            using (TransactionScope transaccion = new TransactionScope())
            {

                try
                {
                    
                    mensaje = "Se elimino correctamente la información.";

                    transaccion.Complete();

                    resultado = "OK";

                }
                catch (Exception ex)
                {
                    transaccion.Dispose();

                    mensaje = ex.Message;
                    resultado = "ER";

                }

            }
            return Json(new { contenido = mensaje, indicador = resultado });
        }

        public ActionResult VerDetalleProducto(int idProducto) {


            try
            {

                List<Producto> lista_producto;
                List<Especificacion_tecnica> lita_espec_tecnica;
                Cotizacion objCotizacion = new Cotizacion();

                lista_producto = objBusqueda.BuscarProductoxId(idProducto);

                objProductoViewModel.lstProducto = lista_producto;
                ViewBag.lstProductoSelect = lista_producto;

                return View(objProductoViewModel);

            }
            catch (Exception ex)
            {
                return View("~/Views/Shared/Error.cshtml");

            }



        }

    }
}