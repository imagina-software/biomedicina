﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Biomedicina.Repository.Biomedicina;
using Biomedicina.Models;
using System.Web.Script.Serialization;

namespace Biomedicina.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


        //[HttpPost]
        //public JsonResult Index(string Prefix)
        //{

        //    JavaScriptSerializer _serializer = new JavaScriptSerializer();
        //    List<Producto> lista;
        //    String mensaje = "";
        //    String resultado = "";
        //    BusquedaAccess objBusqueda = new BusquedaAccess();




        //    try
        //    {
        //        lista = objBusqueda.ListarUsuario(Prefix);

        //        mensaje = _serializer.Serialize(lista.ToList().FirstOrDefault());
        //        resultado = "OK";

        //    }
        //    catch (Exception ex) {

        //        mensaje = ex.Message;
        //        resultado = "ER";
        //    }


         
        //    return Json(new { data = mensaje, indicador = resultado }, JsonRequestBehavior.AllowGet);
        //}


        [HttpPost]
        public JsonResult Index(string prefix)
        {
            List<Producto> lista;
            BusquedaAccess objBusqueda = new BusquedaAccess();

            lista = objBusqueda.ListarProducto(prefix);

            return Json(lista.ToList()); ;
        }

        [HttpPost]
        public JsonResult AutoComplete_Productos(string Prefix)
        {
            List<Producto> lista;
            BusquedaAccess objBusqueda = new BusquedaAccess();

            lista = objBusqueda.ListarProducto(Prefix);

            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AutoComplete_CategoriaProductos(string Prefix)
        {
            List<Producto> lista;
            BusquedaAccess objBusqueda = new BusquedaAccess();

            lista = objBusqueda.ListarCategoria(Prefix);

            return Json(lista, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult AutoComplete_Servicios(string prefix)
        {
            List<Producto> lista;
            BusquedaAccess objBusqueda = new BusquedaAccess();

            lista = objBusqueda.ListarProducto(prefix);



            return Json(lista);
        }

        public ActionResult ResultadosProducto(Producto objProducto)
        {
            ProductoViewModel objProductoViewModel = new ProductoViewModel();


            List<Producto> lista;
            BusquedaAccess objBusqueda = new BusquedaAccess();

            lista = objBusqueda.BuscarProductoxCategoria(objProducto);

            objProductoViewModel.lstProducto = lista;

            //return View("ResultadosProducto", objProductoViewModel);
            return View(objProductoViewModel);
        }

    }
}