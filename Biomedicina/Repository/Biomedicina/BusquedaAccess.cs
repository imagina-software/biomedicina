﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Biomedicina.Models;
using System.Web;

namespace Biomedicina.Repository.Biomedicina
{
    public class BusquedaAccess
    {

        public List<Producto> ListarProducto(string Prefix)
        {
            var lista = new List<Producto>();
            var dt = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("dbo.BIO_ListarProducto_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@pPrefix", Prefix);

                    con.Open();
                    dt.Load(cmd.ExecuteReader());
                    con.Close();

                    lista = dt.AsEnumerable().Select(x => new Producto
                    {
                        Id = x.Field<short>("id_producto"),
                        Nombre = x.Field<string>("nombre"),
                        Id_categoria=x.Field<short>("id_categoria_pro")


                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                lista = new List<Producto>();
            }

            return lista;
        }



        public List<Producto> BuscarProductoxCategoria(Producto objProducto)
        {
            var lista = new List<Producto>();
            var dt = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("dbo.BIO_BuscarProductoxCategoria_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@pCategoria", objProducto.Id_categoria);

                    con.Open();
                    dt.Load(cmd.ExecuteReader());
                    con.Close();

                    lista = dt.AsEnumerable().Select(x => new Producto
                    {
                        Id = x.Field<short>("id_producto"),
                        Nombre = x.Field<string>("nombre"),
                        Id_categoria = x.Field<short>("id_categoria_pro"),
                        Nombre_categoria = x.Field<string>("nombre_categoria"),
                        Nombre_empresa = x.Field<string>("nombre_empresa"),
                        Imagen_url = x.Field<string>("imagen_url")



                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                lista = new List<Producto>();
            }

            return lista;
        }


        public List<Producto> ListarCategoria(string Prefix)
        {
            var lista = new List<Producto>();
            var dt = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("dbo.BIO_ListarCategoria_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@pPrefix", Prefix);

                    con.Open();
                    dt.Load(cmd.ExecuteReader());
                    con.Close();

                    lista = dt.AsEnumerable().Select(x => new Producto
                    {
                        Id_categoria = x.Field<short>("id_categoria_pro"),
                        Nombre_categoria = x.Field<string>("nombre")


                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                lista = new List<Producto>();
            }

            return lista;
        }


        public List<Empresa> ListarEmpresa()
        {
            var lista = new List<Empresa>();
            var dt = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("dbo.BIO_ListarEmpresa_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    //cmd.Parameters.AddWithValue("@pCategoria", objProducto.Id_categoria);

                    con.Open();
                    dt.Load(cmd.ExecuteReader());
                    con.Close();

                    lista = dt.AsEnumerable().Select(x => new Empresa
                    {
                        Id = x.Field<short>("id_empresa"),
                        Razon_social = x.Field<string>("razon_social")


                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                lista = new List<Empresa>();
            }

            return lista;
        }


        public List<Producto> FiltrarProductoXEmpresa(int idEmpresa, int idCategoria)
        {
            var lista = new List<Producto>();
            var dt = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("dbo.BIO_FiltrarProductoXEmpresa_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@pIdEmpresa", idEmpresa);
                    cmd.Parameters.AddWithValue("@pIdCategoria", idCategoria);


                    con.Open();
                    dt.Load(cmd.ExecuteReader());
                    con.Close();

                    lista = dt.AsEnumerable().Select(x => new Producto
                    {
                        Id = x.Field<short>("id_producto"),
                        Nombre = x.Field<string>("nombre"),
                        Id_categoria = x.Field<short>("id_categoria_pro"),
                        Nombre_empresa=x.Field<string>("nombre_empresa"),
                        Nombre_categoria=x.Field<string>("nombre_categoria")


                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                lista = new List<Producto>();
            }

            return lista;
        }


        public List<Producto> BuscarProductoxId(int IdProducto)
        {
            var lista = new List<Producto>();
            var dt = new DataTable();

            try
            {
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {
                    SqlCommand cmd = new SqlCommand("dbo.BIO_BuscarProductoxId_L", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.AddWithValue("@pIdProducto", IdProducto);

                    con.Open();
                    dt.Load(cmd.ExecuteReader());
                    con.Close();

                    lista = dt.AsEnumerable().Select(x => new Producto
                    {
                        Id = x.Field<short>("id_producto"),
                        Nombre = x.Field<string>("nombre"),
                        Id_categoria = x.Field<short>("id_categoria_pro"),
                        Nombre_categoria = x.Field<string>("nombre_categoria"),
                        Modelo = x.Field<string>("modelo"),
                        Nombre_empresa = x.Field<string>("razon_social"),
                        Descripcion = x.Field<string>("descripcion"),
                        Url_ficha_tecnica = x.Field<string>("url_ficha_tecnica"),
                        Capacidad = x.Field<string>("capacidad"),
                        Dimension = x.Field<string>("dimension"),
                        Energia = x.Field<string>("energia"),
                        Potencia = x.Field<string>("potencia"),
                        Temperatura = x.Field<string>("temperatura"),
                        Temporizador = x.Field<string>("temporizador")

                    })
                    .ToList();
                }
            }
            catch (Exception ex)
            {
                lista = new List<Producto>();
            }

            return lista;
        }



    }
}