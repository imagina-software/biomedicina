﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using Biomedicina.Models;
using System.Web;

namespace Biomedicina.Repository.Biomedicina
{
    public class RegistroAccess
    {

        public string RegistrarCotizacion(ProductoViewModel objProducto)
        {
            string codigo = "";

            string mensaje = string.Empty;


            try
            {

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("dbo.BIO_RegistrarCotizacion_I", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@pIdCLiente", SqlDbType.SmallInt).Value = objProducto.IdUsuario;
                    cmd.Parameters.Add("@pIdProducto", SqlDbType.SmallInt).Value = objProducto.IdProducto;
                    cmd.Parameters.Add("@pMensaje", SqlDbType.VarChar).Value = objProducto.Mensaje;

                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;





                    con.Open();
                    codigo = (String)cmd.ExecuteScalar();

                    mensaje = cmd.Parameters["@MsgTrans"].Value.ToString();


                    con.Close();

                    return mensaje;
                }


            }
            catch (Exception ex)
            {

                //throw;
                //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                throw new Exception { Source = ex.ToString() };

            }

        }

        public string RegistrarCliente(ProductoViewModel objCliente)
        {
            string codigo = "";
            string mensaje = string.Empty;
     
            try
            {

                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("dbo.BIO_RegistrarCliente_I", con);

                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add("@pNombre", SqlDbType.VarChar).Value = objCliente.Cliente.Nombre;
                    cmd.Parameters.Add("@pApellidoPaterno", SqlDbType.VarChar).Value = objCliente.Cliente.ApPaterno;
                    cmd.Parameters.Add("@pApellidoMaterno", SqlDbType.VarChar).Value = objCliente.Cliente.ApMaterno;
                    cmd.Parameters.Add("@pCorreo", SqlDbType.VarChar).Value = objCliente.Cliente.UserName;
                    cmd.Parameters.Add("@pTelefono", SqlDbType.VarChar).Value = objCliente.Cliente.Telefono;
                    cmd.Parameters.Add("@pNombreUsuario", SqlDbType.VarChar).Value = objCliente.Cliente.UserName;
                    //cmd.Parameters.Add("@pPassword", SqlDbType.VarChar).Value = objCliente.Cliente.Password;
                    cmd.Parameters.Add("@pActivo", SqlDbType.Bit).Value = true;
                    cmd.Parameters.Add("@MsgTrans", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;
                    cmd.Parameters.Add("@MsgRespuesta", SqlDbType.VarChar, 100).Direction = ParameterDirection.Output;


                    con.Open();
                    codigo = (String)cmd.ExecuteScalar();

                    mensaje= cmd.Parameters["@MsgRespuesta"].Value.ToString();

                    con.Close();
                }

                return mensaje;
            }
            catch (Exception ex)
            {

                //throw;
                //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                throw new Exception { Source = ex.ToString() };

            }

        }
    }
}