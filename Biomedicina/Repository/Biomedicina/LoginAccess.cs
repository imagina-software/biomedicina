﻿using System;
using System.Collections.Generic;
using Biomedicina.Models;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace Biomedicina.Repository.Biomedicina
{
    public class LoginAccess
    {

        public IEnumerable<Cliente> Login(string pUserName, string pPassword)
        {

            string lineagg = "0";

            try
            {

                List<Cliente> lstUserProfile = new List<Cliente>();
                lineagg += ",1";
                using (SqlConnection con = new SqlConnection(Data.Data.StrCnx_WebsSql))
                {

                    SqlCommand cmd = new SqlCommand("dbo.BIO_LeeClientePassword_S", con);

                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@pUserName", SqlDbType.VarChar).Value = pUserName;
                    cmd.Parameters.Add("@pPassword", SqlDbType.VarChar).Value = pPassword;

                    lineagg += ",2";
                    con.Open();
                    cmd.ExecuteNonQuery();
                    SqlDataReader rdr = cmd.ExecuteReader();
                    lineagg += ",3";
                    while (rdr.Read())
                    {
                        lineagg += ",4";
                        if (rdr["usuario"].ToString().Trim() == pUserName.Trim())
                        {

                            Cliente fuserprofile = new Cliente
                            {
                                UserId = Convert.ToInt32(rdr["id_cliente"]),
                                UserName = rdr["usuario"].ToString(),
                                Nombre = rdr["Nombre"].ToString(),
                                ApPaterno = rdr["apellido_paterno"].ToString(),
                                ApMaterno = rdr["apellido_materno"].ToString()
                            };

                            lstUserProfile.Add(item: fuserprofile);

                        }

                    }
                    lineagg += ",5";
                    con.Close();
                }

                return lstUserProfile;

            }
            catch (Exception ex)
            {

                //throw;
                //Bitacora.Current.Error<LoginAccess>(ex, new { lineagg });
                //return new List<Cliente> { new Cliente { EmailCliente = lineagg, NomCliente = ex.Message } };
                throw new Exception { Source = ex.ToString() };

            }

        }
    }
}